package com.example.routes


import com.example.model.Comentario
import com.example.model.Pelicula
import com.example.model.cartellera
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.carteleraRouting(){
    route("/cartelera") {
        get {
            if (cartellera.isNotEmpty()) call.respond(cartellera)
            else call.respondText("No se ha encontado ninguna pelicula", status = HttpStatusCode.OK)
        }
        get("{id?}"){
            if (call.parameters["id"].isNullOrBlank()){
                return@get call.respondText("No se ha encontrado ninguna pelicula con la id ${call.parameters["id"]}", status = HttpStatusCode.BadRequest)
            }
            for (pelicula in cartellera){
                if(pelicula.id == call.parameters["id"]) return@get call.respond(pelicula)
            }
            call.respondText("No se ha encontrado la id ${call.parameters["id"]} en la cartelera.", status = HttpStatusCode.NotFound)
        }
        post {
            val pelicula = call.receive<Pelicula>()
            pelicula.id = (cartellera.size + 1).toString()
            cartellera.add(pelicula)
            call.respondText("Pelicula añadida a la cartelera", status = HttpStatusCode.Created)
        }
        post("{id?}") {
            if (call.parameters["id"].isNullOrBlank()){
                return@post call.respondText("Id de la pelicula incorrecto.", status = HttpStatusCode.BadRequest)
            }
            val id = call.parameters["id"]
            for (pelicula in cartellera) {
                if (pelicula.id == id) {
                    val comentari = call.receive<Comentario>()
                    comentari.id = (pelicula.comentarios.size + 1).toString()
                    comentari.id_pelicula = id
                    pelicula.comentarios.add(comentari)
                    return@post call.respondText("Comentario añadido a la pelicula  ${pelicula.titulo}",status = HttpStatusCode.Created )
                }
            }
            call.respondText("Pelicula no encontrada", status = HttpStatusCode.NotFound)
        }
        put("{id?}") {
            if (call.parameters["id"].isNullOrBlank()){
                return@put call.respondText("Id de la pelicula incorrecto", status = HttpStatusCode.BadRequest)
            }
            val id = call.parameters["id"]
            val peliculaPerModificar = call.receive<Pelicula>()
            for (pelicula in cartellera) {
                if (pelicula.id == id) {
                    pelicula.titulo = peliculaPerModificar.titulo
                    pelicula.anio = peliculaPerModificar.anio
                    pelicula.genero = peliculaPerModificar.genero
                    pelicula.director = peliculaPerModificar.director
                    return@put call.respondText(
                        "Se ha actualizado correctamente la pelicula ${pelicula.titulo}",
                        status = HttpStatusCode.Accepted
                    )
                }
            }
            call.respondText("No se ha encontrado la pelicula con id $id en la cartelera", status = HttpStatusCode.NotFound)
        }
        delete("{id?}") {
            if (call.parameters["id"].isNullOrBlank()){
                return@delete call.respondText("La id de la pelicula no es correcta", status = HttpStatusCode.BadRequest)
            }
            val id = call.parameters["id"]
            for (pelicula in cartellera) {
                if (pelicula.id == id) {
                    cartellera.remove(pelicula)
                    return@delete call.respondText("${pelicula.titulo} ha sido eliminada de la cartelera", status = HttpStatusCode.Accepted)
                }
            }
            call.respondText("Id no encontrada", status = HttpStatusCode.NotFound)
        }
        get("{id?}/comentarios"){
            if (call.parameters["id"].isNullOrBlank()){
                return@get call.respondText("Id de pelicula incorrecto", status = HttpStatusCode.BadRequest)
            }
            for (pelicula in cartellera){
                if(pelicula.id == call.parameters["id"]){
                    return@get call.respond(pelicula.comentarios)
                }
            }
            call.respondText("No existen comentarios para la pelicula ${call.parameters["id"]}.", status = HttpStatusCode.NotFound)
        }



    }
}