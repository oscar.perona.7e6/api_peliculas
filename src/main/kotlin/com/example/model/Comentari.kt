package com.example.model

data class Comentario(
    var id: String,
    var id_pelicula: String,
    val comentario: String,
    val fecha: String
)
