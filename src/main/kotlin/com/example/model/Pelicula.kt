package com.example.model

data class Pelicula(
    var id: String,
    var titulo: String,
    var anio: String,
    var genero: String,
    var director: String,
    val comentarios: MutableList<Comentario>
)

val cartellera = mutableListOf<Pelicula>()
